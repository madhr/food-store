/**
 * Created by Magda on 01.04.2016.
 */
public class ZamowienieBuilder{

    private static final String wielkosciZup[] = {"600ml", "500ml", "400ml", "300ml", "200ml"};

    public Zamowienie dajDanieWegetarianskie(){
        Zamowienie zamowienie = new Zamowienie();
        zamowienie.dodajElement(new Penne());
        zamowienie.dodajElement((Pomidorowa)FabrykaZup.dajPomidorowa(wielkosciZup[0]));
        zamowienie.dodajElement((Pomidorowa)FabrykaZup.dajPomidorowa(wielkosciZup[0]));
        zamowienie.dodajElement((Pomidorowa)FabrykaZup.dajPomidorowa(wielkosciZup[1]));
        zamowienie.dodajElement((Pomidorowa)FabrykaZup.dajPomidorowa(wielkosciZup[2]));
        zamowienie.dodajElement((Pomidorowa)FabrykaZup.dajPomidorowa(wielkosciZup[2]));
        return zamowienie;
    }

    public Zamowienie dajDanieZMiesem(){
        Zamowienie zamowienie = new Zamowienie();
        zamowienie.dodajElement(new Spaghetti());
        zamowienie.dodajElement((Barszcz)FabrykaZup.dajBarszcz(wielkosciZup[0]));
        return zamowienie;
    }

    private static String dajLosowaWielkoscZupy(){
        return wielkosciZup[(int)Math.random()* wielkosciZup.length];
    }
}