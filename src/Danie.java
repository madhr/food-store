/**
 * Created by Magda on 01.04.2016.
 */
public interface Danie {
    public String nazwa();
    public Opakowanie sposobPodania();
    public float cena();
}