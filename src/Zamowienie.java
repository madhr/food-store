/**
 * Created by Magda on 01.04.2016.
 */
import java.util.ArrayList;
import java.util.List;

public class Zamowienie{

    String typ;

    private List<Danie> dania = new ArrayList<Danie>();


    public void dodajElement(Danie danie){
        dania.add(danie);
    }

    public float dajCene(){
        float cost = 0.0f;

        for (Danie danie : dania) {
            cost += danie.cena();
        }
        return cost;
    }

    public void pokazSkladniki(){

        for (Danie danie : dania) {
            System.out.print("Danie : " + danie.nazwa());
            System.out.print(", Opakowanie : " + danie.sposobPodania().typOpakowania());
            System.out.println(", Cena : " + danie.cena());
        }
    }
}