/**
 * Created by Magda on 01.04.2016.
 */
public class Pomidorowa extends Zupa {

    private String wielkosc;

    public Pomidorowa(String wielkosc){
        this.wielkosc = wielkosc;
    }

    @Override
    public float cena() {
        return 30.0f;
    }

    @Override
    public String nazwa() {
        return "Pomidorowa";
    }
}