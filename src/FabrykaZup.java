import java.util.HashMap;

/**
 * Created by Magda on 03.04.2016.
 */
public class FabrykaZup {

    private static final HashMap<String, Zupa> mapaBarszczy = new HashMap<>();
    private static final HashMap<String, Zupa> mapaPomidorowych = new HashMap<>();

    public static Zupa dajBarszcz(String wielkosc){
        Barszcz barszcz = (Barszcz) mapaBarszczy.get(wielkosc);

        if(barszcz==null){
            barszcz = new Barszcz(wielkosc);
            mapaBarszczy.put(wielkosc,barszcz);
            System.out.println("tworzenie nowego obiektu Barszcz o rodzaju: "+wielkosc);
        }
        return barszcz;
    }

    public static Zupa dajPomidorowa(String wielkosc){
        Pomidorowa pomidorowa = (Pomidorowa) mapaPomidorowych.get(wielkosc);

        if(pomidorowa==null){
            pomidorowa = new Pomidorowa(wielkosc);
            mapaPomidorowych.put(wielkosc,pomidorowa);
            System.out.println("tworzenie nowego obiektu Pomidorowa o rodzaju: "+wielkosc);
        }
        return pomidorowa;
    }
}
