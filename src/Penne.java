/**
 * Created by Magda on 01.04.2016.
 */
public class Penne extends Makaron {

    @Override
    public float cena() {
        return 25.0f;
    }

    @Override
    public String nazwa() {
        return "Penne";
    }
}