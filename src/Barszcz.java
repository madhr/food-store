/**
 * Created by Magda on 01.04.2016.
 */
public class Barszcz extends Zupa {

    private String wielkosc;

    public Barszcz(String wielkosc){
        this.wielkosc = wielkosc;
    }

    @Override
    public float cena() {
        return 35.0f;
    }

    @Override
    public String nazwa() {
        return "Barszcz";
    }
}