public class Main {


    public static void main(String[] args) {

        ZamowienieBuilder zamowienieBuilder = new ZamowienieBuilder();

        Zamowienie vegZamowienie = zamowienieBuilder.dajDanieWegetarianskie();
        System.out.println("\nZamówienie wegetariańskie");
        vegZamowienie.pokazSkladniki();
        System.out.println("Cena całkowita: " + vegZamowienie.dajCene()+"\n");

        System.out.println("==========================================");

        Zamowienie nonVegZamowienie = zamowienieBuilder.dajDanieZMiesem();
        System.out.println("\nZamówienie z mięsem");
        nonVegZamowienie.pokazSkladniki();
        System.out.println("Cena całkowita: " + nonVegZamowienie.dajCene()+"\n");

    }
}