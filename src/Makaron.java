/**
 * Created by Magda on 01.04.2016.
 */
public abstract class Makaron implements Danie {

    @Override
    public Opakowanie sposobPodania() {
        return new Talerz();
    }

    @Override
    public abstract float cena();
}