/**
 * Created by Magda on 01.04.2016.
 */
public abstract class Zupa implements Danie {

    @Override
    public Opakowanie sposobPodania() {
        return new Miska();
    }

    @Override
    public abstract float cena();
}