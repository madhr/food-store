/**
 * Created by Magda on 01.04.2016.
 */
public class Spaghetti extends Makaron {

    @Override
    public float cena() {
        return 50.5f;
    }

    @Override
    public String nazwa() {
        return "Spaghetti";
    }
}